package id

import (
	"math/rand"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

// StringWithCharset generates a string with specified length and charset.
// The length can be any valid int and the charset can be any valid string.
// It will always return a string.
func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// String generates a string with specified length with default charset.
// The length can be any valid int. It will always return a string.
func String(length int) string {
	return StringWithCharset(length, charset)
}
