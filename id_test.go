package id

import (
	"testing"
)

func TestString(t *testing.T) {
	var strs []string

	for i := 0; i < 100; i++ {
		s := String(100)
		for _, n := range strs {
			if s == n {
				t.Error("String already generated")
			}
		}
		strs = append(strs, s)
	}
}
